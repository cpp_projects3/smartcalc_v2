.PHONY: clean clang leaks uninstall run dvi

CC = g++
WWW = -Wall -Wextra -Werror -std=c++17 -pedantic
FLAGS= -lgtest -lgtest_main -lpthread
PATH_TEST = ./test_model/test_model.cpp ./test_model/main.cpp ./model/smartCalculModel.cpp
PATH_APP = ./build/
APP=$(PATH_APP)Calcul.app

SOURCES = ./model/smartCalculModel.cpp \
			./controller/smartCalculModel.cpp \
			./view/main.cpp \
			./view/mainwindow.cpp \
			./view/qcustomplot.cpp \
			./view/stylehelper.cpp

HEDERS = ./model/smartCalculModel.h \
			./controller/smartCalculModel.h \
			./view/mainwindow.h \
			./view/qcustomplot.h \
			./view/stylehelper.h

all: install tests gcov_report dvi dist

install: uninstall
	mkdir $(PATH_APP)
	cd $(PATH_APP) && qmake ../view/Calcul.pro
	make -C $(PATH_APP)

run:
	$(PATH_APP)/Calcul.app/Contents/MacOS/Calcul;

uninstall: clean
	rm -rf ./build

dvi:
	doxygen Doxyfile
	open ./html/index.html

dist:
	@rm -rf Archive_SmartCalc_v2.0/
	@mkdir Archive_SmartCalc_v2.0/
	@mkdir Archive_SmartCalc_v2.0/src
	@cp ./build/Calcul.app/Contents/MacOS/Calcul* Archive_SmartCalc_v2.0/src/
	@tar cvzf Archive_SmartCalc_v2.0.tgz Archive_SmartCalc_v2.0/
	@rm -rf Archive_SmartCalc_v2.0/
tests:
	$(CC) $(WWW) $(PATH_TEST) $(FLAGS) -o ./test_model/test_result
	./test_model/test_result

gcov_report:
	$(CC) $(WWW) -lgcov -coverage $(PATH_TEST) $(FLAGS) -o ./test_model/test_result
	./test_model/test_result
	lcov --no-external -t "test_result" -o test.info -c -d .
	genhtml -o test_result test.info
	@cd ./test_result ; open index.html
	rm -rf *.gcno *.gcda

style_clang:
	cp ../materials/linters/.clang-format ./.clang-format
	clang-format -n --verbose model/*.h model/*.cpp
	clang-format -n --verbose controller/*.h controller/*.cpp
	clang-format -n --verbose view/*.h view/*.cpp
	clang-format -i --verbose controller/*.h controller/*.cpp
	clang-format -i --verbose view/*.h view/*.cpp
	clang-format -i --verbose model/*.h model/*.cpp

leaks:
	leaks --atExit -- ./test_model/test_result

clean:
	rm -rf ./test_model/test_result
	rm -rf ./test_result
	rm -rf ./test.info
	rm -rf .qmake.stash
	rm -rf ./.clang-format
	rm -rf ./*gcda
	rm -rf ./html/
	rm -rf Archive_SmartCalc_v2.0.tgz
	rm -rf .clang-format
