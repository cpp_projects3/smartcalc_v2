![](image/title.jpeg)

## Contents
1. [Install SmartCalc_v2.0](#Part-1.-Install-SmartCalc_v2.0)
2. [How to use](#Part-2.-How-to-use)
3. [A little about the implementation](#Part-3.-A-little-about-the-implementation)

## Part 1. Install SmartCalc_v2.0
- Open console;
- Go to the directory where the "Makefile" is located;
- Write command in console "make install";
- And after installation "make run".

## Part 2. How to use
![](image/calc_show.gif)

Use as a standart calculator. \
Graph settings: \
This is for customizing the rendering of the function. \
You can set the rendering animation and animation speed.

## Part 3. A little about the implementation
- **Арифметические операторы**:
  - Brackets: "()"
  - Addition: "+"
  - Subtraction: "-"
  - Multiplication: "*"
  - Division: "/"
  - Power: "^"
  - Modulus: "mod"
  - Unary minus: "-x"

- **Functions**:
  - cosine: cos(x)
  - sine: sin(x)
  - tangent: tan(x)
  - arc cosine: acos(x)
  - arc sine: asin(x)
  - arc tangent: atan(x)
  - square root: sqrt(x)
  - natural logarithm: ln(x)
  - common logarithm: log(x)