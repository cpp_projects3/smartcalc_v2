#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  s21::MainWindow w;
  w.show();
  //  Проверяет состояние завршение самой программы. Возращает число, индикатор
  //  ошибки.
  return a.exec();
}
